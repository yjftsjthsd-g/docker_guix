# GNU Guix in a container


## Use

```
docker build -t guix .
docker run --privileged --rm --name guixtest -d guix
docker exec -ti guixtest sh
docker container kill guixtest
```
Or instead of building yourself, you can use `registry.gitlab.com/yjftsjthsd-g/docker_guix:latest`


## License

The contents of this repo are under the GNU GPL version 3 or later (see
 LICENSE), but bear in mind that this repo only contains build instructions for
 creating a Docker image and that image will contain software under other
 licenses.
 
I Am Not A Lawyer, user beware.

