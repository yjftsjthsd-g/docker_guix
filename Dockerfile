FROM alpine
ARG guixversion=1.3.0
RUN apk add -U wget xz shadow
RUN wget -q -O /opt/guix.tar.xz https://ftp.gnu.org/gnu/guix/guix-binary-$guixversion.x86_64-linux.tar.xz
# TODO should gpg verify
WORKDIR /
RUN tar xf /opt/guix.tar.xz
#RUN tar --warning=no-timestamp -xf /opt/guix.tar.xz
RUN mkdir -p /root/.config/guix
RUN ln -sf /var/guix/profiles/per-user/root/current-guix /root/.config/guix/current
ENV GUIX_PROFILE=/root/.config/guix/current/user
# TODO source $GUIX_PROFILE/etc/profile somehow - or maybe /usr/local/bin obviates that?

RUN groupadd --system guixbuild
RUN useradd -g guixbuild -G guixbuild -d /var/empty -s `command -v nologin` -c "Guix build user 0" --system guixbuilder0
# for i in `seq -w 1 10`;
# do
#   useradd -g guixbuild -G guixbuild           \
#           -d /var/empty -s `which nologin`    \
#           -c "Guix build user $i" --system    \
#           guixbuilder$i;
# done

RUN mkdir -p /usr/local/bin
RUN ln -s /var/guix/profiles/per-user/root/current-guix/bin/guix /usr/local/bin/guix

RUN guix archive --authorize < /root/.config/guix/current/share/guix/ci.guix.gnu.org.pub

# run the daemon
CMD /root/.config/guix/current/bin/guix-daemon --build-users-group=guixbuild

